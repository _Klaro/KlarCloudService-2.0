/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.authentication.interfaces;

import de.klarcloudservice.netty.authentication.enums.AuthenticationType;
import de.klarcloudservice.netty.channel.ChannelHandler;
import de.klarcloudservice.netty.packet.Packet;
import io.netty.channel.ChannelHandlerContext;

/**
 * @author _Klaro | Pasqual K. / created on 19.10.2018
 */

public interface AuthenticationManager {
    void handleAuth(AuthenticationType authenticationType, Packet packet, ChannelHandlerContext channelHandlerContext, ChannelHandler channelHandler);
}
