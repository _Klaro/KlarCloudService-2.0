/*
  Copyright © 2019 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.proxy.configuration;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

/**
 * @author _Klaro | Pasqual K. / created on 15.01.2019
 */

@AllArgsConstructor
@Getter
public final class Tablist implements Serializable {
    private static final long serialVersionUID = 4140051502165436987L;

    private boolean activated;
    private int refreshPerSecond;
    private String upper_line, lower_line;
}
