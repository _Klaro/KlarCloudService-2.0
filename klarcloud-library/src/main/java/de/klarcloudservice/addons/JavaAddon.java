/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.addons;

import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.addons.configuration.AddonClassConfig;
import de.klarcloudservice.addons.loader.AddonMainClassLoader;
import lombok.Data;

/**
 * @author _Klaro | Pasqual K. / created on 10.12.2018
 */

@Data
public abstract class JavaAddon<E> {
    public abstract E getInternalKlarCloudSystem();

    public abstract KlarCloudLibraryService getInternalKlarCloudLibrary();

    private AddonClassConfig addonClassConfig;
    private AddonMainClassLoader addonMainClassLoader;

    public void onModuleClazzPrepare() {
    }

    public void onModuleLoading() {
    }

    public void onModuleReadyToClose() {
    }

    public String getModuleName() {
        return addonClassConfig != null ? addonClassConfig.getName() : "Module-" + KlarCloudLibrary.THREAD_LOCAL_RANDOM.nextLong();
    }
}
