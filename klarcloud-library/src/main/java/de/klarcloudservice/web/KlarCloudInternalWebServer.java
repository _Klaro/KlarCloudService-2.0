/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.web;

import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.utility.cloudsystem.KlarCloudAddresses;
import de.klarcloudservice.web.handler.KlarCloudInternalWebServerHandler;
import de.klarcloudservice.web.utils.WebHandlerAdapter;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.SelfSignedCertificate;
import lombok.Getter;

import javax.net.ssl.SSLException;
import java.io.File;
import java.security.cert.CertificateException;

/**
 * @author _Klaro | Pasqual K. / created on 30.10.2018
 */

@Getter
public class KlarCloudInternalWebServer {
    protected EventLoopGroup bossGroup = KlarCloudLibrary.eventLoopGroup();
    protected EventLoopGroup workerGroup = KlarCloudLibrary.eventLoopGroup();
    private ServerBootstrap serverBootstrap;
    private SslContext sslContext;

    private final WebHandlerAdapter webHandlerAdapter = new WebHandlerAdapter();

    /**
     * Creates a new WebServer instance and binds it to the given Port and IP, provided
     * by the {@link KlarCloudAddresses}.
     *
     * @param klarCloudAddresses            Ip and Port where the WebServer will be bound to
     * @param ssl                           If ssl should be enabled or not
     * @param certFile                      SSL-Certificate file, if this is {@code null} a
     *                                      {@link SelfSignedCertificate} will be created
     *                                      and used
     * @param keyFile                       SSL-Certificate key-file if this is {@code null}
     *                                      a {@link SelfSignedCertificate} will be created
     *                                      and used
     * @throws CertificateException         If the Certificate cant be created
     * @throws SSLException                 If the Certificate cant be used
     * @throws InterruptedException         If the Cloud cannot bind the Webservice
     */
    public KlarCloudInternalWebServer(KlarCloudAddresses klarCloudAddresses, boolean ssl, final File certFile, final File keyFile) throws CertificateException, SSLException, InterruptedException {
        KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("Trying to bind WebServer @" + klarCloudAddresses.getHost() + ":" + klarCloudAddresses.getPort() + "...");

        if (ssl) {
            if (certFile == null || keyFile == null) {
                KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("Trying to create SelfSignedCertificate...");
                SelfSignedCertificate selfSignedCertificate = new SelfSignedCertificate();
                sslContext = SslContextBuilder.forServer(selfSignedCertificate.key(), selfSignedCertificate.cert()).build();
                KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("SelfSignedCertificate has been created");
            } else {
                KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("Trying to load SSL certificate...");
                sslContext = SslContextBuilder.forServer(certFile, keyFile).build();
                KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("SSL certificate has been loaded");
            }
        }

        serverBootstrap = new ServerBootstrap()
                .group(bossGroup, workerGroup)

                .childOption(ChannelOption.IP_TOS, 24)
                .childOption(ChannelOption.AUTO_READ, true)
                .childOption(ChannelOption.TCP_NODELAY, true)

                .channel(KlarCloudLibrary.klarCloudServerSocketChanel())

                .childHandler(new ChannelInitializer<Channel>() {
                    @Override
                    protected void initChannel(Channel channel) {
                        if (sslContext != null && ssl)
                            channel.pipeline().addLast(sslContext.newHandler(channel.alloc()));

                        channel.pipeline().addLast(new HttpServerCodec(), new HttpObjectAggregator(Integer.MAX_VALUE), new KlarCloudInternalWebServerHandler(webHandlerAdapter));
                    }
                });
        serverBootstrap.bind(klarCloudAddresses.getHost(), klarCloudAddresses.getPort()).sync().channel().closeFuture();

        KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info("Successfully bound WebServer @" + klarCloudAddresses.getHost() + ":" + klarCloudAddresses.getPort());
    }

    /**
     * Closes the WebHandler
     */
    public void shutdown() {
        this.bossGroup.shutdownGracefully();
        this.workerGroup.shutdownGracefully();
    }
}
