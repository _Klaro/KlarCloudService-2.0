/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.commands;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.commands.interfaces.Command;
import de.klarcloudservice.commands.interfaces.CommandSender;
import de.klarcloudservice.utility.StringUtil;

import java.io.IOException;

/**
 * @author _Klaro | Pasqual K. / created on 16.12.2018
 */

public final class CommandClear implements Command {
    @Override
    public void executeCommand(CommandSender commandSender, String[] args) {
        try {
            KlarCloudController.getInstance().getKlarCloudConsoleLogger().getConsoleReader().clearScreen();
        } catch (final IOException ex) {
            StringUtil.printError(KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger(), "Error in clear command", ex);
        }
    }

    @Override
    public final String getPermission() {
        return "klarcloud.command.clear";
    }
}
