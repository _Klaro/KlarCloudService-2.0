/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.utility.signs.configuration;

import com.google.gson.reflect.TypeToken;
import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.exceptions.LoadException;
import de.klarcloudservice.netty.packets.PacketOutUpdateAll;
import de.klarcloudservice.signs.KlarCloudSign;
import de.klarcloudservice.signs.SignLayout;
import de.klarcloudservice.signs.SignLayoutConfiguration;
import de.klarcloudservice.utility.StringUtil;
import de.klarcloudservice.utility.TypeTokenAdaptor;
import de.klarcloudservice.utility.files.FileUtils;
import de.klarcloudservice.utility.signs.netty.packets.PacketOutCreateSign;
import de.klarcloudservice.utility.signs.netty.packets.PacketOutRemoveSign;
import lombok.Getter;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 12.12.2018
 */

@Getter
public class SignConfiguration {
    private final Path path = Paths.get("layout.json");
    private Map<UUID, KlarCloudSign> signMap = KlarCloudLibrary.concurrentHashMap();
    private SignLayoutConfiguration signLayoutConfiguration;

    /**
     * Loads the SignConfiguration
     */
    public void loadAll() {
        if (! Files.exists(Paths.get("klarcloud/signs")))
            FileUtils.createDirectory(Paths.get("klarcloud/signs"));

        if (! Files.exists(Paths.get("klarcloud/signs/" + path))) {
            new Configuration().addProperty("config", new SignLayoutConfiguration(
                    new SignLayout.GroupLayout(
                            new SignLayout(new String[]{"§8§m---------§r", "&c§lmaintenance", " ", "§8§m---------"}, "STAINED_CLAY", 4),
                            new SignLayout(new String[]{"%server%", "&6&l%client%", "%online_players%/%max_players%", "%motd%"}, "STAINED_CLAY", 4),
                            new SignLayout(new String[]{"%server%", "&6&lFULL", "%online_players%/%max_players%", "%motd%"}, "STAINED_CLAY", 4),
                            new SignLayout(new String[]{"%server%", "&6&l%client%", "%online_players%/%max_players%", "%motd%"}, "STAINED_CLAY", 4)

                    ), KlarCloudLibrary.concurrentHashMap(), new SignLayout.LoadingLayout(
                    4, 0,
                    new SignLayout[]{
                            new SignLayout(new String[]{"§0§m-------------", "  Server", " l", "§0§m-------------"}),
                            new SignLayout(new String[]{"§0§m-------------", "  Server", " lo", "§0§m-------------"}),
                            new SignLayout(new String[]{"§0§m-------------", "  Server", " loa", "§0§m-------------"}),
                            new SignLayout(new String[]{"§0§m-------------", "  Server", " load", "§0§m-------------"}),
                            new SignLayout(new String[]{"§0§m-------------", "  Server", " loadi", "§0§m-------------"}),
                            new SignLayout(new String[]{"§0§m-------------", "  Server", " loadin", "§0§m-------------"}),
                            new SignLayout(new String[]{"§0§m-------------", "  Server", " loading", "§0§m-------------"}),
                            new SignLayout(new String[]{"§0§m-------------", "  Server", " loadin", "§0§m-------------"}),
                            new SignLayout(new String[]{"§0§m-------------", "  Server", " loadi", "§0§m-------------"}),
                            new SignLayout(new String[]{"§0§m-------------", "  Server", " load", "§0§m-------------"}),
                            new SignLayout(new String[]{"§0§m-------------", "  Server", " loa", "§0§m-------------"}),
                            new SignLayout(new String[]{"§0§m-------------", "  Server", " lo", "§0§m-------------"}),
                            new SignLayout(new String[]{"§0§m-------------", "  Server", " l", "§0§m-------------"}),
                            new SignLayout(new String[]{"§0§m-------------", "  Server", " ", "§0§m-------------"}),
                            new SignLayout(new String[]{"§0§m-------------", "  Server", " ", "§0§m-------------"})
                    })
            )).saveAsConfigurationFile(Paths.get("klarcloud/signs/" + path));
        }

        this.signLayoutConfiguration = Configuration.loadConfiguration(Paths.get("klarcloud/signs/" + path)).getValue("config", TypeTokenAdaptor.getSignLayoutConfigType());
        this.loadSigns();
    }

    /**
     * Creates a sign
     *
     * @param klarCloudSign
     */
    public void addSign(final KlarCloudSign klarCloudSign) {
        this.signMap.put(klarCloudSign.getUuid(), klarCloudSign);

        KlarCloudController.getInstance().getChannelHandler().sendToAllAsynchronous(new PacketOutCreateSign(klarCloudSign));

        Configuration configuration = Configuration.loadConfiguration(Paths.get("klarcloud/signs/database.json"));
        configuration.addProperty("signs", this.signMap.values());
        configuration.saveAsConfigurationFile(Paths.get("klarcloud/signs/database.json"));
    }

    /**
     * Deletes a sign
     *
     * @param klarCloudSign
     */
    public void removeSign(final KlarCloudSign klarCloudSign) {
        this.signMap.remove(klarCloudSign.getUuid());

        KlarCloudController.getInstance().getChannelHandler().sendToAllAsynchronous(new PacketOutRemoveSign(klarCloudSign));

        Configuration configuration = Configuration.loadConfiguration(Paths.get("klarcloud/signs/database.json"));
        configuration.remove("signs");
        configuration.addProperty("signs", this.signMap.values());
        configuration.saveAsConfigurationFile(Paths.get("klarcloud/signs/database.json"));
    }

    /**
     * Loads all signs
     */
    public void loadSigns() {
        if (! Files.exists(Paths.get("klarcloud/signs/database.json")))
            new Configuration().addProperty("signs", Collections.emptyList()).saveAsConfigurationFile(Paths.get("klarcloud/signs/database.json"));

        Configuration configuration = Configuration.loadConfiguration(Paths.get("klarcloud/signs/database.json"));
        List<KlarCloudSign> klarCloudSigns = configuration.getValue("signs", new TypeToken<List<KlarCloudSign>>() {
        }.getType());

        if (klarCloudSigns == null) {
            StringUtil.printError(KlarCloudController.getInstance().getKlarCloudConsoleLogger(), "Could not load sign database", new LoadException(new IllegalArgumentException("Sign Database broken or not loadable")));
            KlarCloudController.getInstance().getInternalCloudNetwork().setSigns(false);
            KlarCloudController.getInstance().getChannelHandler().sendToAllSynchronized(new PacketOutUpdateAll(KlarCloudController.getInstance().getInternalCloudNetwork()));
            return;
        }

        klarCloudSigns.forEach(e -> this.signMap.put(e.getUuid(), e));
    }
}
