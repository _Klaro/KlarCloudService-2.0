/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.utility.signs;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.utility.runtime.Shutdown;
import de.klarcloudservice.utility.signs.configuration.SignConfiguration;
import de.klarcloudservice.utility.signs.netty.in.PacketInCreateSign;
import de.klarcloudservice.utility.signs.netty.in.PacketInRemoveSign;
import de.klarcloudservice.utility.signs.netty.in.PacketInRequestSigns;
import lombok.Getter;

import javax.management.InstanceAlreadyExistsException;

/**
 * @author _Klaro | Pasqual K. / created on 12.12.2018
 */

@Getter
public class SignSelector implements Shutdown {
    @Getter
    private static SignSelector instance;

    private SignConfiguration signConfiguration;

    /**
     * Creates a new SignSelector instance
     *
     * @throws Throwable
     */
    public SignSelector() throws Throwable {
        if (instance == null)
            instance = this;
        else
            throw new InstanceAlreadyExistsException();

        KlarCloudController.getInstance().getNettyHandler().registerHandler("CreateSign", new PacketInCreateSign());
        KlarCloudController.getInstance().getNettyHandler().registerHandler("RemoveSign", new PacketInRemoveSign());
        KlarCloudController.getInstance().getNettyHandler().registerHandler("RequestSigns", new PacketInRequestSigns());

        this.signConfiguration = new SignConfiguration();
        this.signConfiguration.loadAll();
    }

    /**
     * Shutdowns the SignSelector
     */
    @Override
    public void shutdownAll() {
        instance = null;

        KlarCloudController.getInstance().getNettyHandler().unregisterHandler("CreateSign");
        KlarCloudController.getInstance().getNettyHandler().unregisterHandler("RemoveSign");
        KlarCloudController.getInstance().getNettyHandler().unregisterHandler("RequestSigns");
    }
}
