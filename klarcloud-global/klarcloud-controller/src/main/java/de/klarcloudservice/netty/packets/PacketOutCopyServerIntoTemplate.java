/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.packets;

import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.netty.packet.enums.PacketSender;
import de.klarcloudservice.netty.packet.enums.QueryType;

import java.util.Collections;

/**
 * @author _Klaro | Pasqual K. / created on 16.12.2018
 */

public final class PacketOutCopyServerIntoTemplate extends Packet {
    public PacketOutCopyServerIntoTemplate(final String name, final String type, final String group) {
        super("CopyServerIntoTemplate", new Configuration().addStringProperty("name", name).addStringProperty("type", type).addStringProperty("group", group), Collections.singletonList(QueryType.COMPLETE), PacketSender.CONTROLLER);
    }
}
