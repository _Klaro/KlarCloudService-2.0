/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.in;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;

import java.util.List;

/**
 * @author _Klaro | Pasqual K. / created on 16.12.2018
 */

public class PacketInDispatchConsoleCommand implements NettyAdaptor {
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        KlarCloudController.getInstance().getKlarCloudConsoleLogger().warn("The following command has been executed via packet.");
        KlarCloudController.getInstance().getCommandManager().dispatchCommand(configuration.getStringValue("command"));
    }
}
