/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.packets;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.meta.proxy.ProxyGroup;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.netty.packet.enums.PacketSender;
import de.klarcloudservice.netty.packet.enums.QueryType;

import java.util.Arrays;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 29.10.2018
 */

public final class PacketOutStartProxy extends Packet {
    public PacketOutStartProxy(final ProxyGroup group, final String processName, final UUID proxyProcess, final Configuration configuration, final String id) {
        super("StartProxy",
                new Configuration().addProperty("group", group).addStringProperty("name", processName).addProperty("proxyProcess", proxyProcess).addConfigurationProperty("preConfig", configuration).addIntegerProperty("id", Integer.valueOf(id)),
                Arrays.asList(QueryType.COMPLETE, QueryType.NO_RESULT), PacketSender.CONTROLLER);
        KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("ProxyProcess has been added to queue: " + proxyProcess);

    }
}
