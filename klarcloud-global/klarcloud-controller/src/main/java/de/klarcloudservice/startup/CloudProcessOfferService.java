/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.startup;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.packets.PacketOutStartGameServer;
import de.klarcloudservice.netty.packets.PacketOutStartProxy;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 30.10.2018
 */

public class CloudProcessOfferService implements Runnable {
    @Getter
    private Map<String, String> waiting = KlarCloudLibrary.concurrentHashMap();

    private void offerServers() {
        KlarCloudController.getInstance().getInternalCloudNetwork().getServerGroups().values().forEach(serverGroup -> {
            if (!KlarCloudController.getInstance().getChannelHandler().isChannelRegistered(serverGroup.getClient()))
                return;

            final Collection<String> servers = KlarCloudController.getInstance().getInternalCloudNetwork()
                    .getServerProcessManager().getOnlineServers(serverGroup.getName());
            final Collection<String> waiting = this.getWaiting(serverGroup.getName());

            final int waitingAndOnline = servers.size() + waiting.size();
            final String id = KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().nextFreeServerID(serverGroup.getName());
            final String name = serverGroup.getName() + KlarCloudController.getInstance().getCloudConfiguration().getSplitter() + (Integer.parseInt(id) <= 9 ? "0" : "") + id;

            if (waitingAndOnline < serverGroup.getMinOnline() && (serverGroup.getMaxOnline() > waitingAndOnline || serverGroup.getMaxOnline() == -1)) {
                this.waiting.put(name, serverGroup.getName());
                KlarCloudController.getInstance().getChannelHandler().sendPacketAsynchronous(serverGroup.getClient(),
                        new PacketOutStartGameServer(serverGroup, name, UUID.randomUUID(), new Configuration(), id)
                );
            }
        });
    }

    private void offerProxies() {
        KlarCloudController.getInstance().getInternalCloudNetwork().getProxyGroups().values().forEach(proxyGroup -> {
            if (!KlarCloudController.getInstance().getChannelHandler().isChannelRegistered(proxyGroup.getClient()))
                return;

            final Collection<String> proxies = KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager()
                    .getOnlineProxies(proxyGroup.getName());
            final Collection<String> waiting = this.getWaiting(proxyGroup.getName());

            final int waitingAndOnline = proxies.size() + waiting.size();
            final String id = KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().nextFreeProxyID(proxyGroup.getName());
            final String name = proxyGroup.getName() + KlarCloudController.getInstance().getCloudConfiguration().getSplitter() + (Integer.parseInt(id) <= 9 ? "0" : "") + id;

            if (waitingAndOnline < proxyGroup.getMinOnline() && (proxyGroup.getMaxOnline() > waitingAndOnline || proxyGroup.getMaxOnline() == -1)) {
                this.waiting.put(name, proxyGroup.getName());
                KlarCloudController.getInstance().getChannelHandler().sendPacketAsynchronous(proxyGroup.getClient(),
                        new PacketOutStartProxy(proxyGroup, name, UUID.randomUUID(), new Configuration(), id)
                );
            }
        });
    }

    public Collection<String> getWaiting(final String name) {
        Collection<String> collection = new ArrayList<>();

        for (Map.Entry<String, String> map : this.waiting.entrySet())
            if (map.getValue().equals(name))
                collection.add(map.getKey());

        return collection;
    }

    @Override
    public void run() {
        this.offerServers();
        this.offerProxies();
    }
}
