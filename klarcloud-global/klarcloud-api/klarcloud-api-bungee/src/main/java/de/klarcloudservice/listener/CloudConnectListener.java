/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.listener;

import de.klarcloudservice.KlarCloudAPIBungee;
import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.bootstrap.BungeecordBootstrap;
import de.klarcloudservice.meta.info.ProxyInfo;
import de.klarcloudservice.meta.info.ServerInfo;
import de.klarcloudservice.meta.proxy.ProxyGroup;
import de.klarcloudservice.meta.proxy.configuration.Tablist;
import de.klarcloudservice.netty.packets.PacketOutLoginPlayer;
import de.klarcloudservice.netty.packets.PacketOutLogoutPlayer;
import de.klarcloudservice.netty.packets.PacketOutProxyInfoUpdate;
import de.klarcloudservice.netty.packets.PacketOutSendControllerConsoleMessage;
import de.klarcloudservice.utility.StringUtil;
import lombok.AllArgsConstructor;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.*;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * @author _Klaro | Pasqual K. / created on 03.11.2018
 */

public final class CloudConnectListener implements Listener {
    public CloudConnectListener() {
        if (KlarCloudAPIBungee.getInstance().getProxyInfo().getProxyGroup().getProxyConfig().getTablist().isActivated()) {
            ThreadImpl thread = new ThreadImpl(KlarCloudAPIBungee.getInstance().getProxyInfo().getProxyGroup().getProxyConfig().getTablist().getRefreshPerSecond());
            thread.setDaemon(true);
            thread.start();
        }
    }

    @EventHandler(priority = - 127)
    public void handle(final ServerConnectEvent event) {
        if (event.getPlayer().getServer() == null) {
            final ServerInfo serverInfo = KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getServerProcessManager().nextFreeLobby(event.getPlayer().getPermissions());
            if (serverInfo != null)
                event.setTarget(ProxyServer.getInstance().getServerInfo(serverInfo.getCloudProcess().getName()));
            else event.setCancelled(true);
        }
    }

    @EventHandler(priority = - 128)
    public void handle(final LoginEvent event) {
        ProxyInfo proxyInfo = KlarCloudAPIBungee.getInstance().getProxyInfo();
        if (proxyInfo.getProxyGroup().getProxyConfig().isMaintenance()
                && ProxyServer.getInstance().getPlayer(event.getConnection().getUniqueId()) != null
                && ! ProxyServer.getInstance().getPlayer(event.getConnection().getUniqueId()).hasPermission("klarcloud.join.maintenance")) {
            event.setCancelReason(TextComponent.fromLegacyText(KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getMessage("internal-api-bungee-maintenance-join-no-permission")));
            event.setCancelled(true);
            return;
        } else if (proxyInfo.getProxyGroup().getProxyConfig().isMaintenance()
                && ! KlarCloudAPIBungee.getInstance().getInternalCloudNetwork()
                .getProxyGroups().get(proxyInfo.getProxyGroup().getName()).getProxyConfig().getWhitelist()
                .contains(event.getConnection().getUniqueId())) {
            event.setCancelReason(TextComponent.fromLegacyText(KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getMessage("internal-api-bungee-maintenance-join-no-permission")));
            event.setCancelled(true);
            return;
        }

        event.setCancelled(false);
        proxyInfo.getOnlinePlayers().add(event.getConnection().getUniqueId());
        KlarCloudAPIBungee.getInstance().getChannelHandler().sendPacketAsynchronous("KlarCloudController", new PacketOutLoginPlayer(event.getConnection().getUniqueId()), new PacketOutProxyInfoUpdate(proxyInfo), new PacketOutSendControllerConsoleMessage("Player [Name=" + event.getConnection().getName() + "/UUID=" + event.getConnection().getUniqueId() + "/IP=" + event.getConnection().getAddress().getAddress().getHostAddress() + "] is now connected"));
    }

    @EventHandler(priority = - 128)
    public void handle(final PlayerDisconnectEvent event) {
        KlarCloudAPIBungee.getInstance().getProxyInfo().getOnlinePlayers().remove(event.getPlayer().getUniqueId());
        KlarCloudAPIBungee.getInstance().getChannelHandler().sendPacketAsynchronous("KlarCloudController", new PacketOutLogoutPlayer(event.getPlayer().getUniqueId()), new PacketOutProxyInfoUpdate(KlarCloudAPIBungee.getInstance().getProxyInfo()), new PacketOutSendControllerConsoleMessage("Player [Name=" + event.getPlayer().getName() + "/UUID=" + event.getPlayer().getUniqueId() + "/IP=" + event.getPlayer().getAddress().getAddress().getHostAddress() + "] is now disconnected"));
    }

    @EventHandler(priority = - 127)
    public void handle(final ServerKickEvent event) {
        final ServerInfo serverInfo = KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getServerProcessManager().nextFreeLobby(event.getPlayer().getPermissions());
        if (serverInfo != null) {
            event.setCancelServer(ProxyServer.getInstance().getServerInfo(serverInfo.getCloudProcess().getName()));
            event.setCancelled(true);
        } else {
            event.setCancelled(false);
            event.setCancelServer(null);
            event.getPlayer().sendMessage(TextComponent.fromLegacyText(KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getMessage("internal-api-bungee-connect-hub-no-server")));
        }
    }

    @EventHandler(priority = - 127)
    public void handle(final ServerSwitchEvent event) {
        this.prepareTablist(event.getPlayer());
    }

    private ProxyGroup getCurrentProxy() {
        return KlarCloudAPIBungee.getInstance().getInternalCloudNetwork().getProxyGroups()
                .get(KlarCloudAPIBungee.getInstance().getProxyInfo().getProxyGroup().getName());
    }

    private void prepareTablist(final ProxiedPlayer proxiedPlayer) {
        final Tablist tablist = this.getCurrentProxy().getProxyConfig().getTablist();

        if (!tablist.isActivated())
            return;

        proxiedPlayer.resetTabHeader();
        proxiedPlayer.setTabHeader(
                new TextComponent(ChatColor.translateAlternateColorCodes('&', tablist.getUpper_line()
                        .replace("%ping%", Integer.toString(proxiedPlayer.getPing()))
                        .replace("%proxy%", KlarCloudAPIBungee.getInstance().getProxyInfo().getCloudProcess().getName())
                        .replace("%server%", proxiedPlayer.getServer().getInfo().getName())
                        .replace("%version%", StringUtil.KLARCLOUD_VERSION)
                        .replace("%client%", KlarCloudAPIBungee.getInstance().getProxyInfo().getCloudProcess().getClient())
                        .replace("%online%", Integer.toString(KlarCloudAPIBungee.getInstance().getProxyInfo().getOnlinePlayers().size()))
                        .replace("%max%", Integer.toString(KlarCloudAPIBungee.getInstance().getProxyInfo().getProxyGroup().getProxyConfig().getMaxPlayers())))),
                new TextComponent(ChatColor.translateAlternateColorCodes('&', tablist.getLower_line()
                        .replace("%ping%", Integer.toString(proxiedPlayer.getPing()))
                        .replace("%proxy%", KlarCloudAPIBungee.getInstance().getProxyInfo().getCloudProcess().getName())
                        .replace("%server%", proxiedPlayer.getServer().getInfo().getName())
                        .replace("%version%", StringUtil.KLARCLOUD_VERSION)
                        .replace("%client%", KlarCloudAPIBungee.getInstance().getProxyInfo().getCloudProcess().getClient())
                        .replace("%online%", Integer.toString(KlarCloudAPIBungee.getInstance().getProxyInfo().getOnlinePlayers().size()))
                        .replace("%max%", Integer.toString(KlarCloudAPIBungee.getInstance().getProxyInfo().getProxyGroup().getProxyConfig().getMaxPlayers()))))
        );
    }

    @AllArgsConstructor
    private final class ThreadImpl extends Thread {
        private final int waitsPerSecond;

        @Override
        public void run() {
            while (true) {
                BungeecordBootstrap.getInstance().getProxy().getPlayers().forEach(CloudConnectListener.this::prepareTablist);
                KlarCloudLibrary.sleep(ThreadImpl.this, 1000 / waitsPerSecond);
            }
        }
    }
}
