/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.in;

import com.google.gson.reflect.TypeToken;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.signaddon.SignSelector;
import de.klarcloudservice.signs.KlarCloudSign;
import de.klarcloudservice.utility.TypeTokenAdaptor;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 12.12.2018
 */

public class PacketInRequestSigns implements NettyAdaptor {
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        SignSelector.getInstance().setSignLayoutConfiguration(configuration.getValue("configuration", TypeTokenAdaptor.getSignLayoutConfigType()));
        SignSelector.getInstance().setSignMap(configuration.getValue("signs", new TypeToken<Map<UUID, KlarCloudSign>>() {
        }.getType()));
    }
}
