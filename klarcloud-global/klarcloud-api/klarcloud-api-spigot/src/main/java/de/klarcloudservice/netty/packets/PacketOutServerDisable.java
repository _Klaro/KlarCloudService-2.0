package de.klarcloudservice.netty.packets;

import de.klarcloudservice.KlarCloudAPISpigot;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.netty.packet.enums.PacketSender;
import de.klarcloudservice.netty.packet.enums.QueryType;

import java.util.Collections;

public class PacketOutServerDisable extends Packet {

    public PacketOutServerDisable() {
        super("ServerDisable", new Configuration().addProperty("serverName", KlarCloudAPISpigot.getInstance().getServerInfo().getCloudProcess().getName()), Collections.singletonList(QueryType.COMPLETE), PacketSender.PROCESS_SERVER);
    }
}
