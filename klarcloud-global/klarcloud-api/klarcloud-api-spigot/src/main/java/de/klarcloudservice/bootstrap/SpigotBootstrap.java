/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.bootstrap;

import de.klarcloudservice.KlarCloudAPISpigot;
import de.klarcloudservice.commands.CommandSelectors;
import de.klarcloudservice.libloader.LibraryLoader;
import de.klarcloudservice.listener.PlayerConnectListener;
import de.klarcloudservice.netty.authentication.enums.AuthenticationType;
import de.klarcloudservice.netty.packets.PacketOutInternalProcessRemove;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 09.12.2018
 */

@Getter
public class SpigotBootstrap extends JavaPlugin {
    @Getter
    public static SpigotBootstrap instance;

    private List<UUID> acceptedPlayers = new ArrayList<>();

    @Override
    public void onLoad() {
        new LibraryLoader().loadJarFileAndInjectLibraries();
        instance = this;
    }

    @Override
    public void onEnable() {
        Arrays.asList(
                new PlayerConnectListener(),
                new CommandSelectors()
        ).forEach(consumer -> this.getServer().getPluginManager().registerEvents(consumer, this));

        CommandSelectors commandSelectors = new CommandSelectors();

        this.getCommand("selectors").setExecutor(commandSelectors);
        this.getCommand("selectors").setPermission("klarcloud.command.selectors");
        this.registerShutdownHook();
        try {
            new KlarCloudAPISpigot();
        } catch (final Throwable throwable) {
            throwable.printStackTrace();
            this.onDisable();
        }
    }

    @Override
    public void onDisable() {
    }

    private void registerShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> KlarCloudAPISpigot.getInstance().getChannelHandler()
                .sendPacketSynchronized("KlarCloudController", new PacketOutInternalProcessRemove(
                        KlarCloudAPISpigot.getInstance().getServerStartupInfo().getUid(), AuthenticationType.SERVER))));
        System.out.println("ServerShutdownHook was added.");
    }
}
