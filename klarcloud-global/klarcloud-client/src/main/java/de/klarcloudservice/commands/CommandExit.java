/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.commands;

import de.klarcloudservice.commands.interfaces.Command;
import de.klarcloudservice.commands.interfaces.CommandSender;

/**
 * @author _Klaro | Pasqual K. / created on 30.10.2018
 */

public final class CommandExit implements Command {
    @Override
    public void executeCommand(CommandSender commandSender, String[] args) {
        commandSender.sendMessage("KlarCloud will stop...");
        System.exit(1);

    }

    @Override
    public String getPermission() {
        return "klarcloud.command.exit";
    }
}
