/*
  Copyright © 2019 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.in;

import de.klarcloudservice.KlarCloudClient;
import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.netty.packets.PacketOutGetLog;
import de.klarcloudservice.serverprocess.startup.CloudServerStartupHandler;
import de.klarcloudservice.serverprocess.startup.ProxyStartupHandler;
import de.klarcloudservice.utility.StringUtil;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * @author _Klaro | Pasqual K. / created on 29.01.2019
 */

public final class PacketInUploadLog implements Serializable, NettyAdaptor {
    private static final long serialVersionUID = 4582766821019976794L;

    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        final String name = configuration.getStringValue("name");
        final String type = configuration.getStringValue("type");

        switch (type) {
            case "bungee": {
                ProxyStartupHandler proxyStartupHandler = KlarCloudClient.getInstance().getCloudProcessScreenService().getRegisteredProxyHandler(name);
                if (proxyStartupHandler != null) {
                    try {
                        String url = proxyStartupHandler.uploadLog();
                        KlarCloudClient.getInstance().getChannelHandler().sendPacketAsynchronous("KlarCloudController",
                                new PacketOutGetLog(url, name));
                    } catch (final IOException ex) {
                        StringUtil.printError(KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger(), "Could not upload log", ex);
                    }
                } else {
                    KlarCloudClient.getInstance().getChannelHandler().sendPacketAsynchronous("KlarCloudController",
                            new PacketOutGetLog(StringUtil.NULL, name));
                }
                break;
            }

            case "spigot": {
                CloudServerStartupHandler cloudServerStartupHandler = KlarCloudClient.getInstance().getCloudProcessScreenService().getRegisteredServerHandler(name);
                if (cloudServerStartupHandler != null) {
                    try {
                        String url = cloudServerStartupHandler.uploadLog();
                        KlarCloudClient.getInstance().getChannelHandler().sendPacketAsynchronous("KlarCloudController",
                                new PacketOutGetLog(url, name));
                    } catch (final IOException ex) {
                        StringUtil.printError(KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger(), "Could not upload log", ex);
                    }
                } else {
                    KlarCloudClient.getInstance().getChannelHandler().sendPacketAsynchronous("KlarCloudController",
                            new PacketOutGetLog(StringUtil.NULL, name));
                }
                break;
            }

            case "client": {
                StringBuilder stringBuilder = new StringBuilder();
                try {
                    Files.readAllLines(Paths.get("klarcloud/logs/CloudLog.0")).forEach(s -> stringBuilder.append(s).append("\n"));
                } catch (final IOException ex) {
                    StringUtil.printError(KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger(), "Could not read log", ex);
                }

                final String url = KlarCloudClient.getInstance().getKlarCloudConsoleLogger().uploadLog(stringBuilder.substring(0));
                KlarCloudClient.getInstance().getChannelHandler().sendPacketAsynchronous("KlarCloudController",
                        new PacketOutGetLog(url, name));
                break;
            }
        }
    }
}
