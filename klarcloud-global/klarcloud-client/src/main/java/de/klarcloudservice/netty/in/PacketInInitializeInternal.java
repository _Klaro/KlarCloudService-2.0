/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.in;

import de.klarcloudservice.KlarCloudClient;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.netty.packet.enums.PacketSender;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.utility.TypeTokenAdaptor;

import java.util.Collections;
import java.util.List;

/**
 * @author _Klaro | Pasqual K. / created on 29.10.2018
 */

public class PacketInInitializeInternal implements NettyAdaptor {
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        if (!queryTypes.contains(QueryType.RESULT) || queryTypes.contains(QueryType.NO_RESULT)) return;

        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("Trying to receive networkProperties...");
        KlarCloudClient.getInstance().setInternalCloudNetwork(configuration.getValue("networkProperties", TypeTokenAdaptor.getInternalCloudNetworkType()));
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info("NetworkProperties are now set and KlarCloudClient is now ready");

        KlarCloudClient.getInstance().getChannelHandler().sendPacketAsynchronous("KlarCloudController", new Packet(
                "AuthSuccess", new Configuration().addStringProperty("name", KlarCloudClient.getInstance().getClientConfiguration().getClientName()),
                Collections.singletonList(QueryType.COMPLETE), PacketSender.CLIENT
        ));
    }
}
