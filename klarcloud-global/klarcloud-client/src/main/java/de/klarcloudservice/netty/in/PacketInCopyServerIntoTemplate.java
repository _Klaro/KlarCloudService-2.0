/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.in;

import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.utility.files.FileUtils;

import java.nio.file.Paths;
import java.util.List;

/**
 * @author _Klaro | Pasqual K. / created on 16.12.2018
 */

public class PacketInCopyServerIntoTemplate implements NettyAdaptor {
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        switch (configuration.getStringValue("type").toLowerCase()) {
            case "server": {
                FileUtils.copyAllFiles(Paths.get("klarcloud/temp/servers/" + configuration.getStringValue("name")), "klarcloud/templates/" + configuration.getStringValue("group"), "spigot.jar");
                break;
            }
            case "proxy": {
                FileUtils.copyAllFiles(Paths.get("klarcloud/temp/proxies/" + configuration.getStringValue("name")), "klarcloud/templates/" + configuration.getStringValue("group"), "BungeeCord.jar");
                break;
            }
            default:
                break;
        }
    }
}
