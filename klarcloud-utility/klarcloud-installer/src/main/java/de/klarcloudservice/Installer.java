/*
  Copyright © 2019 Pasqual K. | All rights reserved
 */

package de.klarcloudservice;

import de.klarcloudservice.utility.StringUtil;
import de.klarcloudservice.utility.files.DownloadManager;
import de.klarcloudservice.utility.files.FileUtils;

import java.io.File;
import java.nio.file.Paths;

/**
 * @author _Klaro | Pasqual K. / created on 15.01.2019
 */

public final class Installer {
    public static void main(final String... args) {
        KlarCloudLibrary.sendHeader();
        System.out.println();
        System.out.println("Trying to pre-install KlarCloudService");

        for (File directory : new File[] {
                new File("KlarCloudController"),
                new File("KlarCloudClient")
        })
        directory.mkdirs();

        DownloadManager.downloadSilent("https://dl.klarcloudservice.de/update/latest/KlarCloudService-Controller.jar", "KlarCloudController/KlarCloudService-Controller.jar");
        DownloadManager.downloadSilent("https://dl.klarcloudservice.de/update/latest/KlarCloudService-Client.jar", "KlarCloudClient/KlarCloudService-Client.jar");

        System.out.println("Download completed");

        final boolean windows = StringUtil.OS_NAME.contains("Windows");
        if (windows) {
            System.out.println("Detected Windows, creating \"start.bat\"...");
            FileUtils.writeToFile(Paths.get("KlarCloudController/start.bat"), "java -XX:+UseG1GC -XX:MaxGCPauseMillis=50 -XX:+UseCompressedOops -Xmx512m -Xms256m -jar KlarCloudService-Controller.jar");
            FileUtils.writeToFile(Paths.get("KlarCloudClient/start.bat"), "java -XX:+UseG1GC -XX:MaxGCPauseMillis=50 -XX:+UseCompressedOops -Xmx512m -Xms256m -jar KlarCloudService-Client.jar");
        } else {
            System.out.println("Detected Linux, creating \"start.sh\"...");
            FileUtils.writeToFile(Paths.get("KlarCloudController/start.sh"), "screen -S KlarCloudController java -XX:+UseG1GC -XX:MaxGCPauseMillis=50 -XX:+UseCompressedOops -Xmx512m -Xms256m -jar KlarCloudService-Controller.jar");
            FileUtils.writeToFile(Paths.get("KlarCloudClient/start.sh"), "screen -S KlarCloudClient java -XX:+UseG1GC -XX:MaxGCPauseMillis=50 -XX:+UseCompressedOops -Xmx512m -Xms256m -jar KlarCloudService-Client.jar");
        }

        System.out.println("KlarCloud has been installed successfully.");
        System.out.println("Deleting this now");
        KlarCloudLibrary.sleep(1000);

        FileUtils.deleteOnExit(new File(FileUtils.getInternalFileName()));
        System.exit(1);
    }
}
