/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.examples.command;

import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.commands.interfaces.CommandSender;

/**
 * @author _Klaro | Pasqual K. / created on 27.12.2018
 */

public class CommandSenderExample implements CommandSender {
    /**
     * How do you want to send a message to this sender
     * Dont use {@link System#out}
     */
    @Override
    public void sendMessage(String message) {
        if (KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger() != null)
            KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger().info(message);
        else
            throw new IllegalStateException("KlarCloudConsole Logger is null or not ready");
    }

    /**
     * Returns if a sender has a specific permission
     * You can use for example a {@link java.util.List<String>} with the permissions
     * For more Information
     * @see de.klarcloudservice.commands.defaults.KlarCloudUserCommandSender
     */
    @Override
    public boolean hasPermission(String permission) {
        return false;
    }
}
